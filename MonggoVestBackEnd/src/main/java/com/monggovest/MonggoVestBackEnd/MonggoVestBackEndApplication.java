package com.monggovest.MonggoVestBackEnd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonggoVestBackEndApplication {

	public static void main(String[] args) {

		SpringApplication.run(MonggoVestBackEndApplication.class, args);
	}

}

