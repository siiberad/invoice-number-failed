package com.monggovest.MonggoVestBackEnd.repository;


import com.monggovest.MonggoVestBackEnd.model.TransactionModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

@RestResource(exported = false)
public interface TransactionRepository extends JpaRepository<TransactionModel, Integer> {
//    Optional<TransactionRepository> findByIdAndUserId(Integer trxId, Integer userId);
}
