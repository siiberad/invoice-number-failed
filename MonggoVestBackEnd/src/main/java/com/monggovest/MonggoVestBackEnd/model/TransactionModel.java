package com.monggovest.MonggoVestBackEnd.model;



import com.monggovest.MonggoVestBackEnd.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Random;


@Entity
@Table(name = "Transaction")
public class TransactionModel {

//    @Autowired
//    private RandomNumber randomNumber;
    @Autowired
    private Utils utils;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer trxId;

    @SequenceGenerator(name: "")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "")
    private String trxInvoiceNum;

    @Enumerated(EnumType.ORDINAL)
    private Status status;

    private String namaBank;

    private Long noRekening;

//    @Basic(optional = false)
//    @Temporal(TemporalType.TIMESTAMP)
//    @Column(name = "created_at", nullable = false, updatable = false)
//    private Date InvoiceDate;

//    @ManyToOne(fetch = FetchType.LAZY, optional = false)
//    @JoinColumn(name = "user_id", nullable = false )
//    @OnDelete(action = OnDeleteAction.CASCADE)
//    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "userID")
//    @JsonIdentityReference(alwaysAsId = true)
//    @JsonIgnore
//    private UserModel userModel;
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
//    @JoinColumn(name = "product_id")
//    @OnDelete(action = OnDeleteAction.CASCADE)
//    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "productID")
//    @JsonIdentityReference(alwaysAsId = true)
//    @JsonIgnore
//    private ProductModel productModel;

//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "bank_id")
//    private BankModel bankModel;


    public Integer getTrxId() {
        return trxId;
    }

    public void setTrxId(Integer trxId) {
        this.trxId = trxId;
    }

    public String getTrxInvoiceNum() {
        return trxInvoiceNum;
    }

    public void setTrxInvoiceNum(String trxInvoiceNum) {
        this.trxInvoiceNum = utils.generateTrxInvoiceNum(6);
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getNamaBank() {
        return namaBank;
    }

    public void setNamaBank(String namaBank) {
        this.namaBank = namaBank;
    }

    public Long getNoRekening() {
        return noRekening;
    }

    public void setNoRekening(Long noRekening) {
        this.noRekening = noRekening;
    }
}




